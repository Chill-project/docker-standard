#!/bin/bash

if [ ! $UID == 1000 ]
then
   echo "the current user id (UID) is not 1000. This may lead to unexpected problems during pre-build phase"
fi

function _get_flavor() {

   local flavor="${1%/}"

   if [ ! -d "${flavor}" ]
   then
      echo "the flavor \"${flavor}\" does not exists"
      exit 1
   fi

   echo $flavor
}


# apply the base files to all flavors directories
function apply_base() {
   find -maxdepth 1 -mindepth 1 -type d \
       -not -name '_base' -not -name '.git' \
       -exec cp _base/* {}/. \;
}

# pre-build the flavor
function pre_build() {
   # check the flavor exists and return an error if not
   _get_flavor "${1}"

   flavor="$(_get_flavor ${1})"
   github_token=$2

   # create json for composer auth
   auth="{\"github.com\":\"${github_token}\"}"

   docker run \
      --rm \
      --volume "${PWD}"/${flavor}:/app \
      --env COMPOSER_AUTH="${auth}" \
      -it chill/ci-image \
      /app/pre-build.sh

}

#clean the file for the pre-builded flavor
function clean() {
   # check the flavor exists and return an error if not
   _get_flavor "${1}"

   flavor="$(_get_flavor ${1})"

   rm -rf "${flavor}"/src/code
}

build() {
   # check the flavor exists and return an error if not
   _get_flavor "${1}"

   flavor="$(_get_flavor ${1})"

   branch="$(git rev-parse --abbrev-ref HEAD)"
   flavortag=${flavor//[_]/\-}
   tag="chill/standard:${branch}-${flavortag}"
   echo "the tag \"${tag}\" will be applied to the build"
   docker build --tag "${tag}" "${flavor}" 
}

case $1 in
   apply)
      apply_base
      ;;
   pre-build)
      pre_build $2 $3
      ;;
   clean)
      clean $2
      ;;
   build)
      build $2
      ;;
   *)
      echo "prepare and compile easily chill docker flavors"
      echo ""
      echo "apply"
      echo "pre-build flavor [github-token]"
      echo "clean flavor"
      echo "build flavor"
      echo ""
      echo "apply      copy file in base to all flavor directories"
      echo "pre-build  prepare flavor and download packages (into a docker container)"
      echo "clean      remove prepared flavor"
      echo "build      build the container for flavor. the image will be tag with the current git branch + the flavor"
      ;;
esac

