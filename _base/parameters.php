<?php 

if (!isset($_ENV['ADMIN_PASSWORD'])) {
   throw new \RuntimeException("The paramater ADMIN_PASSWORD is not defined in the container");
}


$cstrong = null;
$secret = isset($_ENV['SECRET']) ? $_ENV['SECRET'] :
   base64_encode(openssl_random_pseudo_bytes(rand(8,15), $cstrong));

if ($cstrong === false) {
   echo 'Warning: the secret generated is not "cryptographically strong" (see http://php.net/manual/en/function.openssl-random-pseudo-bytes.php for details). You should add a "SECRET" environment value to your container. ';
}

$container->setParameter('admin_password', $_ENV['ADMIN_PASSWORD']);
$container->setParameter('secret', $secret);

// setting environment values
// note that those values are also defined by Dockerfile.
// language
$container->setParameter('locale', (isset($_ENV['LOCALE'])) ? $_ENV['LOCALE'] : 'fr' );
// database :
$container->setParameter('database_host', (isset($_ENV['DATABASE_HOST'])) ? $_ENV['DATABASE_HOST'] : 'db' );
$container->setParameter('database_name', (isset($_ENV['DATABASE_NAME'])) ? $_ENV['DATABASE_NAME'] : 'postgres' );
$container->setParameter('database_user', (isset($_ENV['DATABASE_USER'])) ? $_ENV['DATABASE_USER'] : 'postgres' );
$container->setParameter('database_password', (isset($_ENV['DATABASE_PASSWORD'])) ? $_ENV['DATABASE_PASSWORD'] : 'postgres' );
$container->setParameter('database_port', (isset($_ENV['DATABASE_PORT'])) ? $_ENV['DATABASE_PORT'] : 5432 );
// redis
$container->setParameter('redis_host', (isset($_ENV['REDIS_HOST'])) ? $_ENV['REDIS_HOST'] : 'redis' );
$container->setParameter('redis_port', (isset($_ENV['REDIS_PORT'])) ? $_ENV['REDIS_PORT'] : '6379' );
// gelf (for logging)
$container->setParameter('gelf_host', (isset($_ENV['GELF_HOST'])) ? $_ENV['GELF_HOST'] : 'gelf' );
$container->setParameter('gelf_port', (isset($_ENV['GELF_PORT'])) ? $_ENV['GELF_PORT'] : '12201' );

