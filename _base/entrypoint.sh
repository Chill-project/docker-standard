#!/bin/bash

#immediatly exit if a command fails:
set -e

# waiting for the database to be ready
while ! timeout 1 bash -c "cat < /dev/null > /dev/tcp/${DATABASE_HOST}/${DATABASE_PORT}"
do  
  echo "$(date) : waiting one second for database"; 
  sleep 1; 
done

echo "$(date) : the database is ready";

#migrate
php /var/www/chill/app/console doctrine:migrations:migrate --no-interaction --env=prod

#prepare assets
php /var/www/chill/app/console --env=prod assets:install
php /var/www/chill/app/console --env=prod assetic:dump

#prepare cache
php /var/www/chill/app/console --env=prod cache:clear
chown www-data:www-data app/cache/prod -R

php-fpm 

