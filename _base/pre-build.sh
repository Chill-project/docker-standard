#!/bin/bash

# stop on error
set -e

#docker run --rm --volume `pwd`:/app -e SYMFONY_ENV=prod chill/ci-image:latest \
 echo "installing project in 'src/code' directory" \
 
composer create-project --stability=dev --no-dev --no-install  \
         --no-interaction --prefer-dist chill-project/standard ./src/code dev-master 
cp composer.json  ./src/code/composer.json 
cp parameters.yml ./src/code/app/config/parameters.yml 
cp parameters.php ./src/code/app/config/parameters.php 
cp custom.yml     ./src/code/app/config/custom.yml 
cp AppKernel.php ./src/code/app/AppKernel.php 
cp docker-config.prod.yml ./src/code/app/config/config_prod.yml
cp docker-config.dev.yml ./src/code/app/config/config_dev.yml
cp docker-config.test.yml ./src/code/app/config/config_test.yml

echo "installing dependencies"

composer install --working-dir ./src/code --dev --no-interaction 

