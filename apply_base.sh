#!/usr/bin/env bash

find -maxdepth 1 -mindepth 1 -type d  -not -name '_base' -not -name '.git' -exec cp _base/* {}/. \;
