Docker images for Chill
========================

This repository create images for Chill software (http://www.chill.social).

The images are stored in the docker registry, at https://hub.docker.com/r/chill/standard/

The software may produce different images, depending on which bundles are installed.

The documentation for Chill is available at those places : 

- for user (in French) : https://fr.wikibooks.org/wiki/Chill
- for developers : http://docs.chill.social

Understanding the hierarchy
-----------------------------

### `_base` directory

The `_base` directory contains file which are commons to all images :

- `Dockerfile` instruction ;
- `docker-compose.yml` ;
- `docker-config.<env>.yml` file, which contains instruction about configuration in docker images ;
- `entrypoint.sh`, which configure the container on startup (installing assets, applying modifications to the database, ...)
- `nginx.conf` which configure the nginx image used by docker-compose (this file should be used in production) ;
- `parameters.php` and `parameters.yml` which compile configuration for container ;
- `pre-build.sh` instructions, which **prepare** the compilation (see below).

### Modifiying the base files

After modifiyng the base file, you should run `bash apply_base.sh` which will copy the file in `_base` to every directory specific for image.

### directory specific to each image

The instructions specific to each images are stored in a directory where all bundles names are contained in the directory's name, ordered alphabetically.

The specific files are : 

- `composer.json` file, which will select packages for this image ;
- `AppKernel.php` which will enable bundle in Symfony
- `custom.yml` which will add default basic config 

The pre-build step
-------------------

The pre-build step will : 

- download the basic hierarchy of php application in the directory `src/code` ;
- copy the configuration files (`docker-config.<env>.yml`, `composer.json`, ...) ;
- download the required bundles ;

### Usage

0. **If this is __not__ the first time you run pre-build**, you should remove the `src/code` directory, by using 

```
rm -rf src/code
```

**Currently, we use PHP 5.6 and, on newest platform (Ubuntu 16.04), PHP7 is the default version. You should install PHP5.6 as described here http://askubuntu.com/questions/109404/how-do-i-install-different-upgrade-or-downgrade-php-version-in-still-supported. The command `php5.6 --version` should point to a php 5.6 version.**


1. Install, or upgrade, composer :
      - **install** : see https://getcomposer.org/download/ . The command `php5.6 composer.phar` should work ;
      - **upgrade** : run `php5.6 composer.phar selfupdate`


2. Run pre-build.sh :
  
```
/bin/bash pre-build.sh
```

Building
--------

The easiest way is to buil using docker-compose : 

```
# you must run this command in the same directory as you image
docker-compose build
```

Running an image
----------------

The easiest way is to run image using docker-compose :

```
# you must run this command in the same directory as you image
docker-compose up 
```

**warning** this command will run in foreground. Use `docker-compose up -d` to run in background.

The app will be available at http://localhost:8888

The admin password should be `admin` (login `admin`) and is configurable through the `docker-compose.yml` file.

### stopping image

Use `^C` (CTRL + C) or run `docker-compose stop`.

### adding fixtures to this image

This will add random fixtures to this image :

```
# you must run this command in the same directory as you image
docker-compose exec fpm php app/console doctrine:fixtures:load
```

See also above for problem with memory usage.

### adding more memory to image

If you need more memory for php script, you may want to run thos scripts : 

```
# you must run this command in the same directory as you image
docker-compose exec fpm /bin/bash
# a new terminal will be opened in the container
# from inside the container
echo "memory_limit = 1G" | cat - /usr/local/etc/php/php.ini > temp && mv temp /usr/local/etc/php/php.ini
```

### uploading an sql file to the database

1. Identify the current network used by docker-compose, using `docker network ls` (normally, the name of the directory with `_default`
2. Identify the database container name, created by docker-compose, using `docker-compose ps`
3. run the command : 

```
docker run --rm --link <container name for db>:db --volume /path/to/your/file.sql:/data.sql:ro --net <name of network> chill/database psql -f /data.sql -U postgres -h localhost
```

### inspect database 

1. Identify the current network used by docker-compose, using `docker network ls` (normally, the name of the directory with `_default`
2. Identify the database container name, created by docker-compose, using `docker-compose ps`
3. run the command : 

```
docker run --rm --link <container name for db>:db --it --net <name of network> chill/database psql -f /data.sql -U postgres -h localhost
```

This command will open a `psql` prompt.


Uploading to registry
----------------------

TODO

BUG report and enhancements
----------------------------

Use the issue tracker here : https://framagit.org/Chill-project/docker-standard/issues

License
--------

This repository is available under the AGPLv3 License.

